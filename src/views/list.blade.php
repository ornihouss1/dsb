@extends('netforus.dsb.app')
@section('content')
    @if(isset($banner))
        <h3>Edit : </h3>
        {!! Form::model($banner, ['route' => ['banner.update', $banner->id], 'method' => 'patch']) !!}
    @else
        <h3>Add New banner : </h3>
        {!! Form::open(['route' => 'banner.store']) !!}
    @endif
        <div class="form-inline">
            <div class="form-group">
                {!! Form::text('name',null,['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit($submit, ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
    {!! Form::close() !!}
    <hr>
    <h4>banners To Do : </h4>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($banners as $banner)
                <tr>
                    <td>{{ $banner->name }}</td>
                    <td>
                        {!! Form::open(['route' => ['banner.destroy', $banner->id], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a href="{!! route('banner.edit', [$banner->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection