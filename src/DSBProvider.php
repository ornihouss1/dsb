<?php

namespace NetForUs\DSB;

use Illuminate\Support\ServiceProvider;

class DSBProvider extends ServiceProvider
{
      /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/views', 'dsb');
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/netforus/dsb'),
        ]);
    }
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('NetForUs\DSB\BannerController'); 
       
    }

  
}
